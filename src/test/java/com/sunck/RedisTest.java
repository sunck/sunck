package com.sunck;

import com.sunck.common.exception.ResultUtil;
import com.sunck.common.utils.RedisUtils;
import com.sunck.modules.system.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import java.util.*;

/**
 * @author LengChen
 * @version 1.0
 * @date 2020/8/6
 */
@SpringBootTest
public class RedisTest {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedisUtils redisUtils;

    @Test
    public void testRedis() {
        redisTemplate.opsForHash().put("hashValue","map1","map1-1");
        redisTemplate.opsForHash().put("hashValue","map2","map2-2");
        List<Object> hashList = redisTemplate.opsForHash().values("hashValue");
        System.out.println("通过values(H key)方法获取变量中的hashMap值:" + hashList);
    }

    @Test
    public void testRedisq() throws Exception {
        //System.out.println(ResultUtil.getAllEnumByClassName(""));
//        String key = "hash";
//
//        Map<String, Object> map = new HashMap<>();
//        map.put("1", "hello");
//        map.put("3a", "china1=2");
//
//        // 一次性向hash中存放一个map
//        redisTemplate.opsForHash().putAll(key, map);
//        // 单次往hash中存放一个数据
//        redisUtils.setHash(key, "1", "你好");
//
//        // 获取hash下的所有key和value
//        Map<String, Object> resultMap = redisTemplate.opsForHash().entries(key);
//        for (String hashKey : resultMap.keySet()) {
//            System.out.println(hashKey + ": " + resultMap.get(hashKey));
//        }

//        User user = new User();
//        user.setState(2);
//        System.out.println(Objects.equals(user.getState(),2));
//        System.out.println( user.getState() == 2);

    }


}
