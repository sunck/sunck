package com.sunck.modules.system.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sunck.common.base.BaseEntity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户信息
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/4
 */
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 用户编码 */
    private String userCode;

    /** 登录账号 */
    private String loginCode;

    /** 用户昵称 */
    private String userName;

    /** 登录密码 */
    private String password;

    /** 电子邮箱 */
    private String email;

    /** 手机号码 */
    private String mobile;

    /** 办公电话 */
    private String phone;

    /** 用户性别 */
    private String sex;

    /** 头像路径 */
    private String avatar;

    /** 个性签名 */
    private String sign;

    /** 绑定的手机串号 */
    private String mobileImei;

    /** 用户类型 */
    private String userType;

    /** 用户类型引用编号 */
    private String refCode;

    /** 用户类型引用姓名 */
    private String refName;

    /** 管理员类型（0非管理员 1系统管理员  2二级管理员） */
    private String mgrType;

    /** 密码安全级别（0初始 1很弱 2弱 3安全 4很安全） */
    private String pwdSecurityLevel;

    /** 密码最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String pwdUpdateDate;

    /** 密码修改记录 */
    private String pwdUpdateRecord;

    /** 最后登陆IP */
    private String lastLoginIp;

    /** 最后登陆时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String lastLoginDate;

    /** 冻结时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String freezeDate;

    /** 冻结原因 */
    private String freezeCause;

    /** 用户权重（降序） */
    private String userWeight;

    /** 状态（0正常 1删除 2停用 3冻结） */
    private String status;

    /**
     * 角色对象
     */
    private Role role;

    /**
     * 用户对应的角色集合
     */
    private List<Role> roleList;

    /**
     * 部门对象
     */
    private Office office;

    /**
     * 部门集合
     */
    private List<Office> officeList;



    public User(){
        super();
    }

    public User(String userCode) {
        this.userCode = userCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getMobileImei() {
        return mobileImei;
    }

    public void setMobileImei(String mobileImei) {
        this.mobileImei = mobileImei;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getMgrType() {
        return mgrType;
    }

    public void setMgrType(String mgrType) {
        this.mgrType = mgrType;
    }

    public String getPwdSecurityLevel() {
        return pwdSecurityLevel;
    }

    public void setPwdSecurityLevel(String pwdSecurityLevel) {
        this.pwdSecurityLevel = pwdSecurityLevel;
    }

    public String getPwdUpdateDate() {
        return pwdUpdateDate;
    }

    public void setPwdUpdateDate(String pwdUpdateDate) {
        this.pwdUpdateDate = pwdUpdateDate;
    }

    public String getPwdUpdateRecord() {
        return pwdUpdateRecord;
    }

    public void setPwdUpdateRecord(String pwdUpdateRecord) {
        this.pwdUpdateRecord = pwdUpdateRecord;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getFreezeDate() {
        return freezeDate;
    }

    public void setFreezeDate(String freezeDate) {
        this.freezeDate = freezeDate;
    }

    public String getFreezeCause() {
        return freezeCause;
    }

    public void setFreezeCause(String freezeCause) {
        this.freezeCause = freezeCause;
    }

    public String getUserWeight() {
        return userWeight;
    }

    public void setUserWeight(String userWeight) {
        this.userWeight = userWeight;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public List<Office> getOfficeList() {
        return officeList;
    }

    public void setOfficeList(List<Office> officeList) {
        this.officeList = officeList;
    }

    public boolean isAdmin() {
        if("1".equals(this.userCode)){
            return true;
        }
        return false;
    }
}

