package com.sunck.modules.system.entity;

import com.sunck.common.base.BaseEntity;

/**
 * 组织结构部门表  sys_office
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/17
 */
public class Office extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 机构编码 */
    private String officeCode;

    /** 父级编号 */
    private String parentCode;

    /** 所有父级编号 */
    private String parentCodes;

    /** 本级排序号（升序） */
    private String treeSort;

    /** 所有级别排序号 */
    private String treeSorts;

    /** 是否最末级 */
    private String treeLeaf;

    /** 层次级别 */
    private String treeLevel;

    /** 全节点名 */
    private String treeNames;

    /** 机构代码 */
    private String viewCode;

    /** 机构名称 */
    private String officeName;

    /** 机构全称 */
    private String fullName;

    /** 机构类型 */
    private String officeType;

    /** 负责人 */
    private String leader;

    /** 办公电话 */
    private String phone;

    /** 联系地址 */
    private String address;

    /** 邮政编码 */
    private String zipCode;

    /** 电子邮箱 */
    private String email;

    /** 状态（0正常 1删除 2停用） */
    private String status;

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentCodes() {
        return parentCodes;
    }

    public void setParentCodes(String parentCodes) {
        this.parentCodes = parentCodes;
    }

    public String getTreeSort() {
        return treeSort;
    }

    public void setTreeSort(String treeSort) {
        this.treeSort = treeSort;
    }

    public String getTreeSorts() {
        return treeSorts;
    }

    public void setTreeSorts(String treeSorts) {
        this.treeSorts = treeSorts;
    }

    public String getTreeLeaf() {
        return treeLeaf;
    }

    public void setTreeLeaf(String treeLeaf) {
        this.treeLeaf = treeLeaf;
    }

    public String getTreeLevel() {
        return treeLevel;
    }

    public void setTreeLevel(String treeLevel) {
        this.treeLevel = treeLevel;
    }

    public String getTreeNames() {
        return treeNames;
    }

    public void setTreeNames(String treeNames) {
        this.treeNames = treeNames;
    }

    public String getViewCode() {
        return viewCode;
    }

    public void setViewCode(String viewCode) {
        this.viewCode = viewCode;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOfficeType() {
        return officeType;
    }

    public void setOfficeType(String officeType) {
        this.officeType = officeType;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
