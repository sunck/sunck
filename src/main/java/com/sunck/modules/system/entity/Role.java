package com.sunck.modules.system.entity;

import com.sunck.common.base.BaseEntity;

import java.util.List;
import java.util.Set;

/**
 * 用户权限
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/4
 */
public class Role extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 角色编码 */
    private String roleCode;

    /** 角色名称 */
    private String roleName;

    /** 角色分类（高管、中层、基层、其它） */
    private String roleType;

    /** 角色排序（升序） */
    private String roleSort;

    /** 系统内置（1是 0否） */
    private String isSys;

    /** 用户类型（employee员工 member会员） */
    private String userType;

    /** 数据范围设置（0未设置  1全部数据 2自定义数据） */
    private String dataScope;

    /** 适应业务范围（不同的功能，不同的数据权限支持） */
    private String bizScope;

    /** 状态（0正常 1删除 2停用） */
    private String status;

    /**
     * 菜单对象
     */
    private Menu menu;

    /**
     * 菜单集合
     */
    private List<Menu> menuList;

    /**
     * 用户对象
     */
    private User user;

    public Role(){
        super();
    }
    public Role(User user){
        this.user = user;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleSort() {
        return roleSort;
    }

    public void setRoleSort(String roleSort) {
        this.roleSort = roleSort;
    }

    public String getIsSys() {
        return isSys;
    }

    public void setIsSys(String isSys) {
        this.isSys = isSys;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public String getBizScope() {
        return bizScope;
    }

    public void setBizScope(String bizScope) {
        this.bizScope = bizScope;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
