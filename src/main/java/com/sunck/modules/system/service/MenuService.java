package com.sunck.modules.system.service;

import com.sunck.common.base.CrudService;
import com.sunck.modules.system.entity.Menu;
import com.sunck.modules.system.mapper.MenuMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 菜单管理Service
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/17
 */

@Service
public class MenuService extends CrudService<MenuMapper, Menu> {

    /**
     * 根据用户ID获取菜单列表
     * @param menu
     * @return
     */
    public List<Menu> findByUserId(Menu menu) {
        return  mapper.findByUserId(menu);
    }
}
