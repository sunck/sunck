package com.sunck.modules.system.service;


import com.sunck.common.base.CrudService;
import com.sunck.modules.system.entity.Role;
import com.sunck.modules.system.mapper.RoleMapper;
import org.springframework.stereotype.Service;

/**
 * 角色管理service
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/17
 */

@Service
public class RoleService  extends CrudService<RoleMapper, Role> {

}
