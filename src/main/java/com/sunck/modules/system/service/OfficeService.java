package com.sunck.modules.system.service;

import com.sunck.common.base.CrudService;
import com.sunck.modules.system.entity.Office;
import com.sunck.modules.system.mapper.OfficeMapper;
import org.springframework.stereotype.Service;

/**
 * 部门组织机构service
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/17
 */

@Service
public class OfficeService extends CrudService<OfficeMapper, Office> {

}
