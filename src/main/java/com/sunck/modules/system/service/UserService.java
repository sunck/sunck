package com.sunck.modules.system.service;

import com.sunck.common.base.CrudService;
import com.sunck.modules.system.entity.Role;
import com.sunck.modules.system.entity.User;
import com.sunck.modules.system.mapper.RoleMapper;
import com.sunck.modules.system.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService extends CrudService<UserMapper, User> {

    @Resource
    private UserMapper userMapper;

    /**
     * 根据用户名查询User
     * @param loginCode
     * @return
     */
    public User getUser(String loginCode){
        return userMapper.getUserByLoginCode(loginCode);
    }
}
