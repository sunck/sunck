package com.sunck.modules.system.mapper;

import com.sunck.common.base.CrudMapper;
import com.sunck.modules.system.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 用户管理
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/4
 */
public interface UserMapper extends CrudMapper<User> {

    /**
     * 根据用户名获取用户
     * @param loginCode
     * @return
     */
    User getUserByLoginCode(@Param("loginCode") String loginCode);

    /**
     * 获取用户登录失败次数
     * @param user
     * @return
     */
    User getFailNum(User user);
}
