package com.sunck.modules.system.mapper;


import com.sunck.common.base.CrudMapper;
import com.sunck.modules.system.entity.Office;

/**
 * 组织机构管理 数据层
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/4
 */

public interface OfficeMapper extends CrudMapper<Office> {


}
