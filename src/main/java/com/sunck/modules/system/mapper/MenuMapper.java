package com.sunck.modules.system.mapper;

import com.sunck.common.base.CrudMapper;
import com.sunck.modules.system.entity.Menu;

import java.util.List;


/**
 * 菜单管理 数据层
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/4
 */
public interface MenuMapper extends CrudMapper<Menu> {

    /**
     * 根据用户ID获取菜单
     * @param menu
     * @return
     */
    List<Menu> findByUserId(Menu menu);
}
