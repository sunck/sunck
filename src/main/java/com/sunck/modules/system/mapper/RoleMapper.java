package com.sunck.modules.system.mapper;


import com.sunck.common.base.CrudMapper;
import com.sunck.modules.system.entity.Role;

/**
 * 角色管理
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/4
 */
public interface RoleMapper extends CrudMapper<Role> {



}
