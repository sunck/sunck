package com.sunck.modules.system.controller;

import com.github.pagehelper.PageHelper;
import com.sunck.common.base.AjaxResult;
import com.sunck.common.base.BaseController;
import com.sunck.modules.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("User")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @RequestMapping("getUser")
    public AjaxResult getUser(){

        PageHelper.startPage(1,10);

       // return new AjaxResult(userService.getUser());
        return null;
    }

}
