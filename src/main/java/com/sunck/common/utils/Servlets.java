package com.sunck.common.utils;

import com.sunck.common.config.Global;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;

/**
 * Http与Servlet工具类.
 *
 * @author calvin/thinkgem/LengChen
 * @version 1.0
 * @date 2020/8/7
 */
public class Servlets {
    private final static String[] staticFiles = StringUtils.split(Global.webStaticFile,",");

    /**
     * 获取当前请求对象
     * @return
     */
    public static HttpServletRequest getRequest(){
        try{
            return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        }catch(Exception e){
            return null;
        }
    }

    /**
     * 判断访问URI是否是静态文件请求
     * @throws Exception
     */
    public static boolean isStaticFile(String uri){
        if (StringUtils.endsWithAny(uri, staticFiles)
                && !StringUtils.endsWithAny(uri, ".jsp") && !StringUtils.endsWithAny(uri, ".java")){
            return true;
        }
        return false;
    }
}
