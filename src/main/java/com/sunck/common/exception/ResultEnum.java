package com.sunck.common.exception;

/**
 * 返回信息代码配置类
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/10
 */
public enum ResultEnum {
    UNKNOWN_ERROR(500,"系统错误"),
    SUCCESS(200,"操作成功"),

    /**
     * 权限系统错误代码
     */
    SHIRO_ERROR_1(1001,"暂无权限"),
    SHIRO_ERROR_2(1002,"登录失败,验证码错误, 请重试"),
    SHIRO_ERROR_3(1003,"用户名密码不能为空"),
    SHIRO_ERROR_4(1004,"用户名或密码错误"),
    SHIRO_ERROR_5(1005,"该账号已被停用，请联系管理员");



    private int code;
    private String msg;
    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getStringCode() {
        return String.valueOf(this.code);
    }

    public String getMsg() {
        return msg;
    }
}
