package com.sunck.common.exception;

import java.io.Serializable;
import java.util.List;

/**
 * 统一异常返回格式Result
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/10
 */
public class Result implements Serializable {
    private int code;
    private String msg;
    private Object data;
    private List<Object> dataList;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public List<Object> getDataList() {
        return dataList;
    }

    public void setDataList(List<Object> dataList) {
        this.dataList = dataList;
    }
}
