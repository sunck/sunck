package com.sunck.common.exception;

import com.sunck.common.utils.RedisUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author LengChen
 * @version 1.0
 * @date 2020/8/10
 */
@ControllerAdvice
class SunckExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(RedisUtils.class);


    @ExceptionHandler
    @ResponseBody
    public Result ErrorHandler(AuthorizationException e) {
        logger.error("权限异常",e);
        return ResultUtil.error(ResultEnum.SHIRO_ERROR_1);
    }

    @ExceptionHandler
    @ResponseBody
    public Result ErrorHandler(AuthenticationException e) {
        return ResultUtil.error(ResultEnum.SHIRO_ERROR_4);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e) {
        if(e instanceof SunckException) {
            SunckException myException = (SunckException)e;
            return ResultUtil.error(myException.getCode(),myException.getMessage());
        }else{
            logger.error("系统异常",e);
            return ResultUtil.error(ResultEnum.UNKNOWN_ERROR.getCode(),ResultEnum.UNKNOWN_ERROR.getMsg());
        }
    }
}
