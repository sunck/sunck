package com.sunck.common.exception;

/**
 * 自定义异常类，增加code
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/10
 */
public class SunckException extends RuntimeException{
    private int code;

    public SunckException( int code, String message ) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
