package com.sunck.common.base;

import com.sunck.modules.system.entity.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service基类（内含自定义CRUDMapper）
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/17
 */
public abstract class CrudService<D extends CrudMapper<T>, T extends BaseEntity> {
    /**
     * 持久层对象
     */
    @Autowired
    protected D mapper;

    /**
     * 获取单条数据
     * @param id
     * @return
     */
    public T get(String id) {
        return mapper.get(id);
    }

    /**
     * 获取单条数据
     * @param entity
     * @return
     */
    public T get(T entity) {
        return mapper.get(entity);
    }

    /**
     * 查询列表数据
     * @param entity
     * @return
     */
    public List<T> findList(T entity) {
        return mapper.findList(entity);
    }

//    /**
//     * 查询分页数据
//     * @param page 分页对象
//     * @param entity
//     * @return
//     */
//    public Page<T> findPage(Page<T> page, T entity) {
//        entity.setPage(page);
//        page.setList(dao.findList(entity));
//        return page;
//
//    }

    /**
     * 保存数据（插入或更新）
     * @param entity
     */
    @Transactional(readOnly = false)
    public void insert(T entity) {
        entity.preInsert();
        mapper.insert(entity);
    }

    /**
     * 保存数据（插入或更新）
     * @param entity
     */
    @Transactional(readOnly = false)
    public void update(T entity) {
        entity.preUpdate();
        mapper.update(entity);
    }

    /**
     * 删除数据
     * @param entity
     */
    @Transactional(readOnly = false)
    public void delete(T entity) {
        mapper.delete(entity);
    }

}
