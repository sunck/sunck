package com.sunck.common.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sunck.common.utils.StringUtils;
import com.sunck.modules.system.entity.User;
import com.sunck.modules.system.utils.UserUtils;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * ENTITY基类
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/3
 */

public class BaseEntity  implements Serializable {

    private static final long serialVersionUID = 1L;

    //protected String id;        // 实体编号（唯一标识）

    protected String remarks;	// 备注

    protected String createBy;	// 创建者

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date createDate;	// 创建日期

    protected String updateBy;	// 更新者

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date updateDate;	// 更新日期

    protected String delFlag = DEL_FLAG_NORMAL;; 	// 删除标记（0：正常；1：删除；2：审核）

    protected String sqlSelect;//数据库查询字段

    protected String corpCode;//机构code

    protected String corpName;//机构名称

    public BaseEntity() {
        super();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Size(min = 1, max= 1, message = "数据错误")
    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getSqlSelect() {
        return sqlSelect;
    }

    public void setSqlSelect(String sqlSelect) {
        this.sqlSelect = sqlSelect;
    }

    public String getCorpCode() {
        return corpCode;
    }

    public void setCorpCode(String corpCode) {
        this.corpCode = corpCode;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }



    /**
     * 插入之前执行方法，需要手动调用
     */

    public void preInsert(){
        User user = UserUtils.getUser();
        if (StringUtils.isNotBlank(user.getUserCode())){
            this.updateBy = user.getUserCode();
            this.createBy = user.getUserCode();
            this.corpCode = user.getCorpCode();
            this.corpName = user.getCorpName();
        }
        this.updateDate = new Date();
        this.createDate = this.updateDate;
    }

    /**
     * 更新之前执行方法，需要手动调用
     */
    public void preUpdate(){
        User user = UserUtils.getUser();
        if (StringUtils.isNotBlank(user.getUserCode())){
            this.updateBy = user.getUserCode();
        }
        this.updateDate = new Date();
    }


    /**
     * 删除标记（0：正常；1：删除；2：审核；）
     */
    public static final String DEL_FLAG_NORMAL = "0";
    public static final String DEL_FLAG_DELETE = "1";
    public static final String DEL_FLAG_AUDIT = "2";
}
