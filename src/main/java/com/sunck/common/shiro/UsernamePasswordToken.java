package com.sunck.common.shiro;


import com.sunck.common.config.Global;

/**
 * 自定义授权过滤表单类
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/18
 */
public class UsernamePasswordToken  extends org.apache.shiro.authc.UsernamePasswordToken {

    private static final long serialVersionUID = 1L;

    private String validateCode;
    private boolean mobileLogin;
    private String schoolId;
    private String loginType;


    public  UsernamePasswordToken(){
        super();
    }

    public UsernamePasswordToken(String username, char[] password,
                                  boolean rememberMe, String host, String validateCode, boolean mobileLogin) {
        super(username, password,rememberMe, host);
        this.validateCode = validateCode;
        this.mobileLogin = mobileLogin;
        this.loginType = Global.PASSWORD;
    }

    /**免密登录*/
    public UsernamePasswordToken(String username) {
        super(username, "", false, null);
        this.loginType = Global.NOPASSWD;
    }

    /**账号密码登录*/
    public UsernamePasswordToken(String username, String password) {
        super(username, password, false, null);
        this.loginType = Global.PASSWORD;
    }

    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }

    public boolean isMobileLogin() {
        return mobileLogin;
    }

    public void setMobileLogin(boolean mobileLogin) {
        this.mobileLogin = mobileLogin;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }
}
