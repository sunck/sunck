package com.sunck.common.shiro;

import com.sunck.common.utils.RedisUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;


/**
 *自定义授权缓存管理类
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/7
 */
public class RedisCacheManager implements CacheManager {

    @Autowired
    public RedisUtils redisUtils;

    private String cacheKeyPrefix = "shiro_cache_";

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {

        return new RedisCache<K,V>(cacheKeyPrefix + name,redisUtils);
    }

    public String getCacheKeyPrefix() {
        return cacheKeyPrefix;
    }

    public void setCacheKeyPrefix(String cacheKeyPrefix) {
        this.cacheKeyPrefix = cacheKeyPrefix;
    }
}
