package com.sunck.common.config;

import java.util.Map;

/**
 * 全局配置类
 *
 * @author LengChen
 * @version 1.0
 * @date 2020/8/5
 */
public class Global {
    /**
     * 当前对象实例
     */
    private static Global global = new Global();


    /**
     * 显示/隐藏
     */
    public static final String SHOW = "1";
    public static final String HIDE = "0";

    /**
     * 是/否
     */
    public static final String YES = "1";
    public static final String NO = "0";

    /**
     * 对/错
     */
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    /**
     * 登录方式 密码登录/免密登录
     */
    public static final String PASSWORD = "password";
    public static final String NOPASSWD = "nopassword";


    /**
     * shiroCache配置
     */
    public static final String DEFAULT_Code_PARAM = "validateCode";//验证码字段
    public static final String DEFAULT_MOBILE_PARAM = "mobileLogin";//是否手机登录
    public static final String DEFAULT_MESSAGE_PARAM = "message";//输出信息字段


    /**
     * 手机号码格式限制
     */
    public static final String MOBILE_PHONE_NUMBER_PATTERN = "^0{0,1}(13[0-9]|15[0-9]|14[0-9]|18[0-9])[0-9]{8}$";

    /**
     * 静态文件后缀
     */
    public static final String webStaticFile=".css,.js,.png,.jpg,.gif,.jpeg,.bmp,.ico,.swf,.psd,.htc,.crx,.xpi,.exe,.ipa,.apk";

}
