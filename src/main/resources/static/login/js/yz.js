//注册
$('#Login').click(function () {
	var data = $("#loginForm").serialize();
	console.log(data,"登录数据");
	//处理业务逻辑
	$.ajax({
		type:'post',
		url: ctx + 'login',
		data: data,
		success: function(e) {
			console.log(e);
			if(e.code == 200){
				location.reload()
			}else if (e.code == 1002 && $("#isValidateCodeLogin").is(":hidden")){
				$("#isValidateCodeLogin").show();
				$(".imageCode").click();
			}else{
				layer.msg(e.msg, {icon: 5});
				$(".imageCode").click();
			}
		}
	})

})
$(".imageCode").click(function(){
	var url = ctx + "captcha?s=" + Math.random();
	$(".imageCode").attr("src", url);
})
